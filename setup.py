#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 18:40:21 2022

@author: aquiles
"""

from setuptools import setup, find_packages


setup(
    name='Optics11',
    version='1.0',
    packages=find_packages('.'),
    )
