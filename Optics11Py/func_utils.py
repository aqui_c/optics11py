#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 16:47:18 2022

@author: aquiles
"""

import numpy as np
from scipy.special import erf

def gauss(x, *p):
    """ Returns the value of a gaussian function evaluated in point (or array) x with parameters defined in p.

    .. math:: A\cdot \exp(-(x-\mu)^2/(2\cdot \sigma^2))+o

    Parameters
    ----------
    x : array-like
    p : iterable
        The order of the parameters is: A, :math:`\mu`, :math:`\sigma`, o

    Returns
    -------
    out :
        If x is a value, returns a value. If x is an array, returns an array.
    """
    A, mu, sigma, offset = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))+offset


def err_func(x, a, b, z, f):
   return a * erf((x - z)*f) + b