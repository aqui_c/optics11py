#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:32:04 2022

@author: aquiles
"""
import numpy as np
from scipy.optimize import curve_fit
from .func_utils import gauss, err_func
from nptdms import TdmsFile


def open_file(filename):
    if filename.endswith('csv'):
        my_data = np.loadtxt(filename, delimiter=',', dtype=np.int16)
    elif filename.endswith('tdms'):
        file = TdmsFile.open(filename)
        my_data = file['group_1']['405-LALS(Peak)']

    count, bins_count = np.histogram(my_data, bins=3000)

    pdf = count/np.sum(count)
    cdf = np.cumsum(pdf)
    
    return pdf, cdf, bins_count[1:]


def fit_pdf(bins_count, count, p0=None):
    if p0 is None:
        p0 = [2000, 4000, 1000, 0]
    coeff, var_matrix = curve_fit(gauss, bins_count, count, p0)
    
    return coeff, var_matrix


def fit_cdf(bins_count, cdf, p0=None):
    if p0 is None:
        p0 = [1, 0, 5000, .001]
    coeff, var_matrix = curve_fit(err_func, bins_count, cdf, p0)
    return coeff, var_matrix