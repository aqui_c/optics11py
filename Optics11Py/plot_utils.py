#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 17:41:24 2022

@author: aquiles
"""
import matplotlib as mpl
import matplotlib.pyplot as plt

from Optics11Py.func_utils import gauss, err_func
from pathlib import Path


def set_defaults():

    mpl.rcParams.update({
        'figure.figsize': (10, 8),
        'figure.dpi': 72,
        'xtick.labelsize': 16,
        'ytick.labelsize': 16,
        'lines.markersize': 10,
        'axes.labelsize': 20,
        'image.cmap': 'jet',
        })

def plot_cdf(bins_count, cdf, coeff):
    fig = plt.figure()
    plt.plot(bins_count, cdf, '-', color='red', label='CDF')
    plt.plot(bins_count, err_func(bins_count, *coeff), color='blue', label='Fit')
    plt.legend()
    return fig
    
    
def plot_pdf(bins_count, pdf, coeff):
    fig = plt.figure()
    plt.plot(bins_count, pdf, '-', color='red', label='PDF')
    plt.plot(bins_count, gauss(bins_count, *coeff), label='Fit')
    plt.legend()
    return fig


def save_plot(fig, data_filename, output_extension):
    filename_path = Path(data_filename)
    folder = filename_path.parents[0]
    name = filename_path.stem

    fig_name = folder / (name + output_extension)

    fig.savefig(fig_name)