#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 16:48:13 2022

@author: aquiles
"""
import sys

from .fcm_data import open_file, fit_pdf, fit_cdf
from .plot_utils import plot_cdf, plot_pdf, save_plot

filename = sys.argv[1]
# %%
# filename = 'Data/FCS_Files/LALS_180nm_HOBs.csv'
pdf, cdf, bins_count = open_file(filename)
#pdf, cdf, bins_count = open_file('Data/TDMS_Files/16_HS5_361_nm_hollow_silica_beads_dil2E3.tdms')

# %%

coeff_pdf, var_matrix = fit_pdf(bins_count, pdf)

# %%

coeff_cdf, var_matrix = fit_cdf(bins_count, cdf)

# %%

fig = plot_cdf(bins_count, cdf, coeff_cdf)
save_plot(fig, filename, '_cdf.png')

# %%
fig = plot_pdf(bins_count, pdf, coeff_pdf)
save_plot(fig, filename, '_pdf.png')